# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 20:44:59 2021

@author: yipeng.song@hotmail.com
"""

# %% load required libs
import os
import joblib
import pandas as pd
from end2endML.preprocessing import data_preprocessing
from end2endML.automate_modeling_evaluation import automate_modeling
from end2endML.automate_modeling_evaluation import automate_evaluation
import end2endML.utilities as utilities
import end2endML
print(end2endML.__version__)

# %% load and summarize the data
## specificy the path2data and outcome
# regression problem
# path2data = "./data/titanic.csv"
# outcome = "Age"

# binary classification problem
path2data = "./data/titanic.csv"
outcome = "Survived"

# multi-classification problem
# path2data = './data/Iris.csv'; outcome = 'Species'

# read sas data sets
# path2data = "./data/PaySimSample.sas7bdat"
# outcome = "isFlaggedFraud"

## using the combination of data name and outcome as experiment name to save results
name = os.path.basename(path2data).split(".")[0] + "_" + outcome

## load into the data set
data = utilities.read_data(path2data, index_col=0)
# data = data.sample(1000)
print("Data size is {}".format(data.shape))

## summarize the data set
utilities.summarize_data(data, name, max_sample=1000, minimal=True)

# %% preprocess the data set
# hyperparameters in this step
# help(data_preprocessing) to see the meaning
cat_levels_threshold = 15
missing_threshold = 0.5
for_future_test = True
save_results = True
verbose = 0

# preprocess the data
X, y, _, _, saved_preprocess_steps = data_preprocessing(
    data,
    outcome,
    name,
    cat_levels_threshold=cat_levels_threshold,
    missing_threshold=missing_threshold,
    for_future_test=for_future_test,
    verbose=verbose,
    save_results=save_results,
)

print(X.shape, y.shape)
print(saved_preprocess_steps)
# help(saved_preprocess_steps.transform)

# test the saved_preprocess_steps
X_tmp = saved_preprocess_steps.transform(data)
print(X_tmp.shape[1] == X.shape[1])

# %% if some models have already been selected and trained, append rather than
# replace the previous models

# models used in this experiment
models = ["linear", "lasso", "ridge", "elasticNet", "svm", "nn", "gb", "rf"]
# models = ["linear", "lasso"]

target_path = os.path.join("./results", name)
path2result = os.path.join(target_path, "saved_selected_models.joblib")
if os.path.isfile(path2result):
    previous_results = joblib.load(path2result)

    # remove the already saved models
    previous_models = previous_results.keys()
    previous_models = [ele.split("_")[-1] for ele in previous_models]
    trained_models = utilities.intersection(previous_models, models)
    if len(trained_models) > 0:
        print(f"These models {trained_models} have already existed.")
        models = utilities.setdiff(models, previous_models)
        if len(models) == 0:
            print("All the specified models have already trained and saved.")

# %% prepare the training and test set
test_size = 0.2  # split ratio for test set
random_state = None  # seed for splitting the data

# split train and test set
X_train, X_test, y_train, y_test = utilities.split_data(
    X, y, test_size=test_size, random_state=random_state
)

# %% selected and fit the model
# hyperparameters for tuning the this section
scaler = "standard"  # standard scaling for columns
decomposer = None
n_components = None
imbalance = True  # allow imbalance classification when necessary
imbalance_force = False  # force to use imbalance classification
cv = 10  # K-Fold cross validation
cv_force = False  # force the model selection to use K-Fold CV to evaluate the model
n_trials = 30  # number of Bayesian optimization steps
n_jobs = 10  # number of cores used for data analysis
max_iter = 100  # max number of iterations.
verbose = 0  # show log

results = automate_modeling(
    X_train,
    y_train,
    scaler=scaler,
    decomposer=decomposer,
    n_components=n_components,
    models=models,
    imbalance=imbalance,
    imbalance_force=imbalance_force,
    cv=cv,
    cv_force=cv_force,
    n_trials=n_trials,
    n_jobs=n_jobs,
    max_iter=max_iter,
    verbose=verbose,
)
print(results)

# %% evaluate the selected models
eval_cv = False  # whether to evaluate the selected models using K-Fold CV

# test evaluation or cv evaluation or all of them
eval_test = True if test_size > 0.01 else False
if (not eval_cv) and (test_size < 0.01):
    eval_cv = True

# evaluate the models' performance on the test sets or K-Fold CV
models_metrics = automate_evaluation(
    results,
    X_test,
    y_test,
    X,
    y,
    eval_test=eval_test,
    eval_cv=eval_cv,
    cv=cv,
    n_jobs=n_jobs,
)
print("the performance of the selected models are: \n")
print(models_metrics)


# %% save the selected models and evaluation metrics
# save the selected models
# combine current results and previous results
if os.path.isfile(path2result):
    results.update(previous_results)
joblib.dump(results, filename=path2result)

# save metrics on the selected models
path2metrics = os.path.join(target_path, "metrics.csv")
if os.path.isfile(path2metrics):
    models_metrics_old = pd.read_csv(path2metrics, index_col=0)

    # join the current metrics and previous one
    models_metrics = models_metrics_old.join(models_metrics, how="outer")
models_metrics.to_csv(path2metrics)

# save the preprocessing pipelines
path2preprocess = os.path.join(target_path, "saved_preprocess_steps.joblib")
joblib.dump(saved_preprocess_steps, filename=path2preprocess)
