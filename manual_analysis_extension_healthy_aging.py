# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 20:44:59 2021

@author: yipeng
"""

# %% load required libs
import numpy as np
import pandas as pd 
from sklearn import model_selection
import pandas_profiling as pd_profile
from pathlib import Path

# add the ./codes into the search path to load modules in ./codes
import os 
import sys 
os.chdir('/home/yipeng/end2endML/')
print('current working directory is: {}'.format(os.getcwd()))
sys.path.append(os.path.abspath(os.path.join('.', 'codes')))

# self defined procedures
from data_preprocessing import data_preprocessing
from classification_models import train_classification_models
from regression_models import train_regression_models
from evaluation_metrics import binary_evaluation
from evaluation_metrics import regression_evaluation
from utilities import intersection

# %% configure the meta parameters for the data analysis
# what is experiment name?
name = 'biosample'

# load the row data sets
input_file = './data/{}.csv'.format(name) 
data = pd.read_csv(input_file, index_col=0)
print('data size is {}'.format(data.shape))

# set the hyperparameters
n_jobs = 10
cv = 10
scaler = 'standard'
verbose = 1

# %% summarize the data set 
path2report = './results/' + name + '_profile_report.html'
if Path(path2report).is_file():
    print('Report is already exist.')
else:
    profile_report = pd_profile.ProfileReport(
                                        data.sample(min(1000, data.shape[0])))
    profile_report.to_file(path2report) 

# %% def a fun to preprocess the data set 
outcome = "age" # regression task

X, y, feature_names, sample_index = data_preprocessing(data, outcome = outcome)
print(X.shape, y.shape, len(feature_names))

# save the proprocessed data set
np.savez_compressed('./results/{}'.format(name),
                    X = X,
                    y = y,
                    feature_names = feature_names,
                    sample_index = sample_index
                    )

# %% index out the healthy subjects
healthy_idx = pd.read_csv('./data/healthy_idx.csv'.format(name), index_col=0)
kept_healthy_subjects = intersection(sample_index, healthy_idx.index.to_list())
healthy_idx = healthy_idx.loc[kept_healthy_subjects, :]

# using half of the healhy subjects with the target measures to construct the model
model_subjects = healthy_idx.sample(round(0.8*healthy_idx.shape[0])).index

# get the samples for all the healthy_idx_sampled
X = pd.DataFrame(X, index=sample_index, columns=feature_names)
y = pd.Series(y, index=sample_index)
X = X.loc[model_subjects, :].to_numpy()
y = y.loc[model_subjects].to_numpy()

# save the training index 
np.savez_compressed('./results/{}_healthy_subjects_train'.format(name),
                    feature_names = feature_names,
                    train_index = model_subjects,
                    healthy_index = healthy_idx, 
                    full_index = sample_index
                    )
print(X.shape, y.shape)

# %% data splitting before modeling 
# stratified splitting for classification problem 
stratify = None 
if (len(pd.value_counts(pd.Series(y))) < 5):
    stratify = y
    
X_train, X_test, y_train, y_test = model_selection.train_test_split(
    X, y, test_size=0.2, stratify=stratify, random_state=123)

# %% selected and fit the model
n_trials = 30

# check if it is a regression task or a classification task 
task = "regression"
if y.dtype == "object" and len(pd.Series(y).value_counts()) < 5:
    task = "classification"

# modeling according the task
print('The problem is a {} task'.format(task))

if task == "classification":
    # select and fit the classification models
    results = train_classification_models(X_train, y_train,
                                          scaler = 'standard',
                                          cv = cv,
                                          n_jobs = n_jobs,
                                          verbose = verbose) 
elif task == 'regression':
    results = train_regression_models(X_train, y_train,
                                      scaler = scaler,
                                      cv = cv,
                                      n_jobs = n_jobs,
                                      verbose = verbose,
                                      n_trials = n_trials)
print(results)

# %% evaluate the model's performance on the test set 
if task == 'classification':
    models_metrics = binary_evaluation(results, X_test, y_test)
elif task == "regression":
    models_metrics = regression_evaluation(results, X_test, y_test)
print(models_metrics)

# %% save the results
# save the selected models 
from joblib import dump 
dump(results, 
     filename = './results/' + name + '_healthy_saved_selected_models.joblib')

# save the evaluation metrics 
models_metrics.to_csv('./results/' + name + '_healthy_metrics.csv')














# %%
