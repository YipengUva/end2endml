# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 20:44:59 2021

@author: yipeng
"""
# %% load required libs
import os
from end2endML.automate_analysis import automate_analysis

# %% arguments to control the data loading behavior
## path2data and outcome
print("current working directory: {}".format(os.getcwd()))

# regression problem
# path2data = "./data/titanic.csv"
# outcome = "Age"

# binary classification problem
path2data = "./data/titanic.csv"
outcome = "Survived"

# multi-classification problem
# path2data = "./data/Iris.csv"
# outcome = "Species"

# imbalanced classification
# path2data = "./data/credit.csv"
# outcome = "Credit Default"

# classification results for ML quiz
# path2data = './data/classification.csv'; outcome = 'Credit Default'

# regression results for ML quiz
# path2data = './data/regression.csv'; outcome = 'phenotype'

## the the column of the ID in the data
index_col = 0

# %% arguments to control the preprocessing steps
cat_levels_threshold = 15
missing_threshold = 0.5
for_future_test = False
max_sample = 1000

# %% log information & save the model
verbose = 1
save_results = True

# %% arguments to control the training and test sets splitting
test_size = 0.2
random_state = None

# %% arguments to control the selection and fitting of ML models
imbalance = True
imbalance_force = True
scaler = "standard"
decomposer = None
n_components = None
models = ["linear", "lasso", "ridge", "elasticNet", "svm", "nn", "gb", "rf"]
n_trials = 30
n_jobs = 10
max_iter = 100

# %% arguments to control the model evaluations
cv = 10
cv_force = False
eval_cv = False

# %%
# automate data analysis. results are saved in the results folder of
# the current working directory
automate_analysis(
    path2data,
    outcome,
    imbalance=imbalance,
    imbalance_force=imbalance_force,
    index_col=index_col,
    test_size=test_size,
    eval_cv=eval_cv,
    scaler=scaler,
    decomposer=decomposer,
    n_components=n_components,
    models=models,
    cv=cv,
    cv_force=cv_force,
    n_trials=n_trials,
    n_jobs=n_jobs,
    max_iter=max_iter,
    max_sample=max_sample,
    verbose=verbose,
    cat_levels_threshold=cat_levels_threshold,
    missing_threshold=missing_threshold,
    for_future_test=for_future_test,
    save_results=save_results,
    random_state=random_state,
)
