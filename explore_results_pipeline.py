# %% load required packages
import numpy as np
import pandas as pd
import joblib

# make sure you have the correct working directory
import os

print("current working directory is: {}".format(os.getcwd()))
# os.chdir('working directory') # set the wokring directory for runing the code

# %% extract all the saved results
# what is the name of the experiment
name = "titanic_Survived"
target_path = os.path.join("./results", name)

# load into saved models
path2models = os.path.join(target_path, "saved_selected_models.joblib")
saved_models = joblib.load(path2models)
print("saved models:\n", saved_models.keys())

# load into saved metrics
path2results = os.path.join(target_path, "metrics.csv")
saved_metrics = pd.read_csv(path2results)
print("performance of saved models:\n", saved_metrics.head(3))

# load into saved preprocessing steps
path2preprocess = os.path.join(target_path, "saved_preprocess_steps.joblib")
saved_preprocess_steps = joblib.load(path2preprocess)
print(saved_preprocess_steps)

# %% extract the details of a saved model
# what is the model you want to check
model_name = "clf_lasso"

# extract the specific model
saved_model = saved_models[model_name]
print(saved_model)

# the saved model, feature importance, and
# selected hyperparameters are in
# the attributes of saved model
feature_importance = saved_model.feature_importances_
selected_hyperparameter = saved_model.selected_hyperparameters_
searching_range = saved_model.searching_ranges_
selected_model = saved_model.selected_model_

# %% find the top features
# load into the feature names
path2data = os.path.join(target_path, "data.npz")
data_set = np.load(path2data, allow_pickle=True)
feature_names = data_set["feature_names"]
print("feature names:\n", feature_names[0:10])

# show feature importances
print("feature importance shape: \n", feature_importance.shape)
feature_importance = pd.Series(feature_importance, index=feature_names)
order = feature_importance.abs().sort_values(ascending=False)
feature_importance = feature_importance[order.index]
print(feature_importance.head(10))
