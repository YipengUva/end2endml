# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 20:44:59 2021

The command line interface for end2endML project

@author: yipeng.song@hotmail.com
"""

# %% load required libs
import click
from end2endML.automate_analysis import automate_analysis

# %% define a fun to do automate data analysis 
@click.command()
@click.option('--path2data', help='path to the data')
@click.option('--outcome', help='the name of the outcome')
@click.option('--imbalance', default=0, help='allow imbalance classification when necessary')
@click.option('--imbalance_force', default=0, help='force to use imbalance classification')
@click.option('--index_col', default="0", help='the column ID, it can also be None')
@click.option('--test_size', default=0.2, help='the proportion for test set')
@click.option('--eval_cv', default=0, help='using K-Fold CV to evaluate the selected models')
@click.option('--scaler', default='standard', help='scaling method')
@click.option('--models', default="linear, elasticNet, gb, rf", 
            help='a substring of "linear, lasso, ridge, elasticNet, svm, nn, gb, rf"')
@click.option('--cv', default=10, help='the number of folders used in CV')
@click.option('--cv_force', default=0, help='force the model selection to use K-Fold CV to evaluate the model')
@click.option('--n_trials', default=30, help='n_trials for Bayesian optimizaiton')
@click.option('--n_jobs', default=10, help='the number of cores used')
@click.option('--max_iter', default=100, help='max number of iteration')
@click.option('--verbose', default=1, help='print log to the screen or not')
@click.option('--cat_levels_threshold', default=15, 
                help='the threshold to decide if a column is categorical or not')
@click.option('--missing_threshold', default=0.5, 
                help='remove samples or variables when over missing_threshold of their values are missing')
@click.option('--save_results', default=1, help='whether to save the results')
@click.option('--random_state', default="None", help='random seed for splitting train and test.')
def main(
        path2data,
        outcome,
        imbalance = 0,
        imbalance_force = 0,
        index_col = "0",
        test_size = 0.2,
        eval_cv = 0,
        scaler = 'standard',
        models = "linear, elasticNet, gb, rf",    
        cv = 10,
        cv_force = 0,
        n_trials = 30, 
        n_jobs = 10, 
        max_iter = 100, 
        verbose = 1,
        cat_levels_threshold=15,
        missing_threshold=0.5,
        save_results=1,
        random_state="None"):
    # a simple dict to tranasform integer to bool
    int2bool = {
        0: False, 1: True}
    ## transform the arguments to the standard one
    # integer to bool
    imbalance = int2bool[imbalance]
    imbalance_force = int2bool[imbalance_force]
    eval_cv = int2bool[eval_cv]
    cv_force = int2bool[cv_force]
    save_results = int2bool[save_results]

    # other nonstandard transformations
    if index_col == 'None':
        index_col = None 
    else: 
        index_col = int(index_col)
    
    if random_state == 'None':
        random_state = None 
    else: 
        random_state = int(random_state)
    models = models.split(',')
    models = [ele.strip() for ele in models]
    
    # using the automate analysis procedure
    automate_analysis(path2data=path2data, 
                    outcome = outcome,
                    imbalance=imbalance,
                    imbalance_force=imbalance_force, 
                    index_col=index_col,
                    test_size=test_size,
                    eval_cv=eval_cv,
                    scaler=scaler,
                    models=models,
                    cv=cv,
                    cv_force=cv_force,
                    n_trials = n_trials, 
                    n_jobs = n_jobs, 
                    max_iter = max_iter, 
                    verbose = verbose,
                    cat_levels_threshold=cat_levels_threshold,
                    missing_threshold=missing_threshold,
                    save_results=save_results,
                    random_state=random_state)


if __name__ == '__main__':
    main()    
