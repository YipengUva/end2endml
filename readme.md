The end2endML package has already been moved to https://gitlab.com/YipengUva/end2endml_pkg. It is still in testing phase. However, you can already install the package through pip. The usage of the package can be found in the corresponding readme file.

### Contents of end2endML Git repository

- **semi-automate_analysis_pepeline:** Sections in this file explain well what need to done to do semi-automatic data analysis.
- **automate_analysis_cmd.py:**  The command line interface for semi-automate_analysis.py. 
- **automate_analysis_pipeline.py**: script used to test the end2endML package.
- **semi-automate_analysis_dl.py:** Semi-automatic data analysis pipeline using deep learning for the tabular data sets.
- **data:** This fold should contains the data set used.
- **results:** This fold hold the results.
- **dl_results:** This fold hold the results of deep learning models.

### Usage of end2endML package 

The usage can be found in the readme file of https://gitlab.com/YipengUva/end2endml_pkg

### Dependence

Deep learning: pytorch, pytorch_tabular, wandb.

### Example usages of traditional ML models

- We can take semi-automate_analysis.py as a template to do semi-automatic data analysis. 

- We can use  automate_analysis.py to do automate data analysis. An example is shown as follows.


### Deep learning

Two deep learning models, multiple layer perceptron and Neural Oblivious Decision Ensembles, are implemented. Compared to the neural network model in sklearn library, the deep learning models implemented using PyTorch have more advanced features, e.g. batch normalization, drop out, mini-batch, etc. Bayesian optimization is used to do model selection. GPU is always preferred for the training of deep learning models. However, not only according to my experiments but also from previous publications, the deep learning model doesn't have a clear advantage compared to the gradient boosting model on tabular data sets. Neural Oblivious Decision Ensembles is a recently developed method and it was claimed to beat gradient boosting on all their experiments on tabular data sets. The improved performance is not that much on the paper and according to my experiments on my data sets. 

Example usage of deep learning models is as follows: python semi-automate_analysis_dl.py --model_name=mlp --n_trials=100 --n_jobs=10. For the documents of these arguments, using python semi-automate_analysis_dl.py --help. 

