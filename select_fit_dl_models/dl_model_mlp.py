# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 17:56:09 2021

This is a script to select and fit a mlp model using Bayesian optimizaiton

@author: yipeng
"""

#TODO add option for classification models

# %% load required libs
import torch 
from pytorch_tabular import TabularModel
from pytorch_tabular.models import CategoryEmbeddingModelConfig
from pytorch_tabular.config import OptimizerConfig, TrainerConfig, ExperimentConfig

# Bayesian optimizaiton 
import optuna
from optuna import trial

# %% check if cuda is avaliable 
if torch.cuda.is_available():
    gpus = 1
    print('gpu will be used for traning dl model.')
else:
    gpus = 0
    print('cpu will be used for traning dl model.')
    
# %% def a fun to config the model 
def config_mlp(
    data_config, name, 
    task='regression', 
    layers = ["100", "50", "25"],
    learning_rate = 1e-3,
    dropout=0.5, batch_size=512, 
    max_epochs=2000, use_batch_norm=True,
    auto_lr_find=False,
    gpus=1):
    # transform the layers to the target format
    layers_str = '-'.join([str(ele) for ele in layers])

    # configure the trainer, optimizer, model and experiment
    trainer_config = TrainerConfig(
        auto_lr_find=auto_lr_find,
        batch_size=batch_size,
        max_epochs=max_epochs,
        accumulate_grad_batches=1,
        early_stopping='valid_loss',
        early_stopping_patience=10,
        gpus=gpus)
    
    optimizer_config = OptimizerConfig(optimizer='Adam')
    
    model_config = CategoryEmbeddingModelConfig(
        task=task,
        learning_rate=learning_rate,
        loss="MSELoss",
        layers=layers_str,
        activation='ReLU',
        dropout=dropout,
        use_batch_norm=use_batch_norm
    ) 
    
    experiment_name = name + '_mlp'
    run_name = '{}; {}; {}'.format(layers_str, learning_rate, dropout)
    experiment_config = ExperimentConfig(
        project_name=experiment_name,
        run_name=run_name,
        exp_watch='gradients',
        log_target='wandb',
        log_logits=True
    )
    
    tabular_model = TabularModel(
        data_config=data_config,
        model_config=model_config,
        optimizer_config=optimizer_config,
        trainer_config=trainer_config,
        experiment_config=experiment_config
    )
    return tabular_model

# %% def a fun to run the optimization 
def run_mlp_optimization(
        train, val,
        data_config,
        name, task='regression',
        n_trials=100
        ):
    # def the Bayesian optimization objective fun
    def objective(trial):
        # select the layers for mlp
        dropout = trial.suggest_uniform('dropout', 0.0, 0.9)
        learning_rate = trial.suggest_loguniform('learning_rate', 1e-4, 1e-2)
    
        n_layers = trial.suggest_int('n_units_layers', 2, 7, 1) # number of layers
        layers = []
        for i in range(n_layers):
            # number of units
            n_units = trial.suggest_categorical(
                'n_units_layer_{}'.format(i), [16, 32, 64, 128, 256, 512, 1024])
            layers.append(n_units)
    
        # set up and fit the model 
        torch.cuda.empty_cache()
        tabular_model = config_mlp(
            data_config = data_config,
            name=name, task=task,
            layers=layers,
            learning_rate = learning_rate,
            dropout=dropout,
            gpus=gpus
        )
        tabular_model.fit(train=train, validation=val)
        result = tabular_model.evaluate(val)
        val_loss = result[0]['valid_loss']
        return val_loss
    
    # run the Bayesian optimization process
    study = optuna.create_study(
        direction='minimize',
        study_name='{}_mlp'.format(name), 
                            storage='sqlite:///dl_results/{}_mlp.db'.format(name),
                            load_if_exists=True)
    study.optimize(objective, n_trials=n_trials)

    # saved the selected hyper-parameters 
    best_params = study.best_params
    
    # extract layers 
    n_layers = best_params['n_units_layers']
    layers = []
    for i in range(n_layers):
        ith_layer = best_params['n_units_layer_{}'.format(i)]
        layers.append(ith_layer)
    
    # train the selected model 
    selected_model = config_mlp(
        data_config = data_config,
        name = name,
        task = task,
        layers=layers,
        learning_rate=best_params['learning_rate'],
        dropout=best_params['dropout']
        )
    selected_model.fit(train=train, validation=val)
    print("The selected model's performance:")
    selected_result = selected_model.evaluate(val)
    print(selected_result)

    return best_params, selected_model
    
    
    
    
    
    
    
    
    
