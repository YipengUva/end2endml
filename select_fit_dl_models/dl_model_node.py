# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 17:56:09 2021

This is a script to select and fit a mlp model using Bayesian optimizaiton

@author: yipeng
"""

# %% load required libs
import torch 
from pytorch_tabular import TabularModel
from pytorch_tabular.models import NodeConfig
from pytorch_tabular.config import OptimizerConfig, TrainerConfig, ExperimentConfig
from torch_optimizer import QHAdam

# Bayesian optimizaiton 
import optuna
from optuna import trial

# %% check if cuda is avaliable 
if torch.cuda.is_available():
    gpus = 1
    print('gpu will be used for traning dl model.')
else:
    gpus = 0
    print('cpu will be used for traning dl model.')
    
# %% def a fun to config the model 
def config_node(
    data_config, 
    name, 
    task='regression',
    num_layers=2,
    num_trees=1024,
    dropout=0.5,
    depth=6,
    additional_tree_output_dim=0,
    embed_categorical=False,
    learning_rate = 1e-3,
    batch_size=32, 
    max_epochs=10, 
    auto_lr_find=False,
    gpus=1):
 
    # configure the trainer, optimizer, model and experiment
    trainer_config = TrainerConfig(
        auto_lr_find=auto_lr_find,
        batch_size=batch_size,
        max_epochs=max_epochs,
        accumulate_grad_batches=1,
        early_stopping='valid_loss',
        early_stopping_patience=10,
        gpus=gpus)
    
    optimizer_config = OptimizerConfig()
    
    model_config = NodeConfig(
        task=task,
        learning_rate=learning_rate,
        loss="MSELoss",
        num_layers=num_layers,
        num_trees=num_trees,
        depth = depth,
        input_dropout=dropout,
        embed_categorical=embed_categorical
    ) 
    
    experiment_name = name + '_node'
    run_name = '{}; {}; {}; {}'.format(num_trees, num_layers, learning_rate, dropout)
    experiment_config = ExperimentConfig(
        project_name=experiment_name,
        run_name=run_name,
        exp_watch='gradients',
        log_target='wandb',
        log_logits=True
    )
    
    tabular_model = TabularModel(
        data_config=data_config,
        model_config=model_config,
        optimizer_config=optimizer_config,
        trainer_config=trainer_config,
        experiment_config=experiment_config
    )
    return tabular_model

# %% def a fun to run the optimization 
def run_node_optimization(
        train, val,
        data_config,
        name, task='regression',
        n_trials=100
        ):
    # def the Bayesian optimization objective fun
    def objective(trial):
        # select the layers for mlp
        dropout = trial.suggest_uniform('dropout', 0.0, 0.9)
        learning_rate = trial.suggest_loguniform('learning_rate', 1e-4, 1e-2)
    
        num_layers = trial.suggest_int('num_layers', 2, 8, 1)
        num_trees = trial.suggest_categorical(
                'num_trees', [8, 16, 32, 64, 128, 256])
        tree_depth = trial.suggest_categorical('tree_depth', [6, 8])
        additional_tree_output_dim = trial.suggest_int(
            'additional_tree_output_dim', 0, 2, 1)
        
        # set up and fit the model 
        torch.cuda.empty_cache()
        tabular_model = config_node(
             data_config, name, 
             task='regression',
             num_layers=num_layers,
             num_trees=num_trees,
             dropout=dropout,
             depth=tree_depth,
             additional_tree_output_dim=additional_tree_output_dim,
             learning_rate = learning_rate,
             gpus=gpus
            )
        
        tabular_model.fit(train=train, 
                  validation=val,
                  optimizer=QHAdam,
                  optimizer_params={
                      "nus": (0.7, 1.0),
                      "betas": (0.95, 0.998)})
        result = tabular_model.evaluate(val)
        val_loss = result[0]['valid_loss']
        return val_loss
    
    # run the Bayesian optimization process
    study = optuna.create_study(
        direction='minimize',
        study_name='{}_node'.format(name), 
                            storage='sqlite:///dl_results/{}_node.db'.format(name),
                            load_if_exists=True)

    study.optimize(objective, n_trials=n_trials)

    # saved the selected hyper-parameters 
    best_params = study.best_params
    
    # train the selected model 
    selected_model = config_node(
        data_config, name, 
        task='regression',
        num_layers=best_params['num_layers'],
        num_trees=best_params['num_trees'],
        dropout=best_params['dropout'],
        depth=best_params['tree_depth'],
        additional_tree_output_dim=best_params['additional_tree_output_dim'],
        gpus=gpus
        )
    selected_model.fit(train=train, validation=val)
    print("The selected model's performance:")
    selected_result = selected_model.evaluate(val)
    print(selected_result)
    
    return best_params, selected_model
    
    
    
    
    
    
    
    
    
