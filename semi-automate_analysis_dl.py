# -*- coding: utf-8 -*-
"""
Created on Sat Apr 17 20:44:59 2021

@author: yipeng
"""

# %% load required libs
import numpy as np
import pandas as pd 
from sklearn import model_selection
from sklearn.impute import SimpleImputer
import click
from joblib import dump
from sklearn import metrics

# add the ./codes into the search path to load modules in ./codes
import os 
import sys 
#os.chdir('/home/yipeng/end2endML/') # Wokring directory for runing the code
print('current working directory is: {}'.format(os.getcwd()))
sys.path.append(os.path.abspath(os.path.join('.', 'codes')))

# dl model 
from data_preprocessing import data_preprocessing
from dl_model_mlp import run_mlp_optimization
from dl_model_node import run_node_optimization
from pytorch_tabular.config import DataConfig

# Bayesian optimizaiton 
import optuna
from optuna import trial

# %% read into the data set 
# what is experiment name and outcome name?
name = 'biosample'
outcome = 'age'

# load the row data sets
input_file = './data/{}.csv'.format(name) #
data = pd.read_csv(input_file, index_col=0)
print('data size is {}'.format(data.shape))

# %% preprocess the data
X, y, feature_names, sample_index = data_preprocessing(data, outcome = outcome)
del data
print(X.shape, y.shape, len(feature_names))

# save the proprocessed data set for poential further analysis
# check the document of np.savez_compressed to see how to load the saved data
path2data = './dl_results/' + name + '.npz'
if os.path.isfile(path2data):
    print('Data is already saved.')
else: 
    np.savez_compressed(path2data,
                    X = X,
                    y = y,
                    feature_names = feature_names, # features names
                    sample_index = sample_index # index of samples
                    )

# %% prepare the preprocessed for dl model    
# reorgnize X, y into dataframe
data = pd.DataFrame(X, index=sample_index, columns=feature_names)
data[outcome] = y

# extract target columns, categorical variables and numerical variables
target_cols = [outcome]

#TODO Using experiments to see if we need to sepcify dummy codding as 
# categorical variables
unique_cols = np.apply_along_axis(lambda x: len(np.unique(x)), 
                                        axis = 0, 
                                        arr = X)
cat_col_names = pd.Series(feature_names).loc[unique_cols == 2].to_list()
num_col_names = pd.Series(feature_names).loc[unique_cols != 2].to_list()

# %% split train, validation and test set for deep learning models 
train, test = model_selection.train_test_split(data, 
                                               test_size=0.2, 
                                               random_state=123)
train, val = model_selection.train_test_split(train, 
                                              test_size=0.2, 
                                              random_state=123)
print(train.shape, val.shape, test.shape)

# %% select the model 
@click.command()
@click.option('--model_name', default='mlp', help='the name of the model; mlp or node')
@click.option('--n_trials', default=100, help='number of trials during Bayesian optimization')
@click.option('--n_jobs', default=10, help='number of cores used')
def main(model_name = 'mlp', n_trials=10, n_jobs=10):
    # data configuration for dl models
    # TODO experiment with the feature transformer
    data_config = DataConfig(
        target=target_cols,
        continuous_cols=num_col_names,
        categorical_cols=cat_col_names,
        #normalize_continuous_features=True,
        continuous_feature_transform='quantile_normal',
        num_workers=n_jobs
        )
    
    # create dl_results to hold tmp data and results
    if not os.path.isdir('./dl_results'):
        os.mkdir("./dl_results")
    
    # select and fit the selected model
    if model_name == 'mlp':
        best_params, selected_model = run_mlp_optimization(
            train, val, data_config, name, task='regression', n_trials=n_trials)
    elif model_name == 'node':
        # normalize the age
        mu, std = train.age.mean(), train.age.std()
        normalize = lambda x: ((x - mu) / std).astype(np.float32)
        train.age, val.age, test.age = map(normalize, [train.age, val.age, test.age])
        print("mean = %.5f, std = %.5f" % (mu, std))
        
        # select the model
        best_params, selected_model = run_node_optimization(
            train, val, data_config, name, task='regression', n_trials=n_trials)
    
    # evaluate the model's performance 
    test_hat = selected_model.predict(test)
    y_test = test.age
    y_test_hat = test_hat.age_prediction

    # evaluate the model    
    model_metrics = pd.DataFrame(
        {'r2': metrics.r2_score(y_test, y_test_hat),
         'mse': metrics.mean_squared_error(y_test, y_test_hat),
         'mae': metrics.mean_absolute_error(y_test, y_test_hat)
         }, index=['selected_model'])
    print(model_metrics)

    # an example of the prediction
    print('and example of true y and estimated y: ')
    print(pd.concat([y_test[0:5], y_test_hat[0:5]], axis=1))
    
    # save best parameters
    dump(best_params, 
             filename = './dl_results/' + name + \
                 '_best_params_{}.joblib'.format(model_name))

    # save the metrics of the selected model 
    dump(model_metrics,
             filename = './dl_results/' + name + \
                 '_model_metrics_{}.joblib'.format(model_name))

    # save the selected  model
    selected_model.save_model('./dl_results/' + name + \
                              '_savedModel_{}'.format(model_name))

if __name__ == '__main__':
    import datetime
    tic = datetime.datetime.now()
    main()
    toc = datetime.datetime.now()
    print('time used {}'.format(toc - tic))
    



